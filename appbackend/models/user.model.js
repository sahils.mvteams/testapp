const mongoose = require('mongoose');

const userModelSchema = new mongoose.Schema({
	email:{type:String},
	password:{type:String},
	fullName:{type:String},
    phoneNumber:{type:Number},
	gender:{type:String},
	country:{type:String},
	remote_work:{type:String},
	image_path:{type:String},
	resume_path:{type:String},
	extraFiles:{type:Array}
})
const User= mongoose.model('user', userModelSchema)
module.exports = User