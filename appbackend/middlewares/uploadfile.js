const multer = require('multer')
const path = require('path')
const storage =  multer.diskStorage({
    destination:function(req,file,cb){
        console.log("in upload",req)
        if(req.params && req.params.oldfile){
            cb(null,'public/uploads/multiplefiles')
        }else if(req.params && req.params.oldImage){
            cb(null,'public/uploads/images')
        }else{
            let ext = file.mimetype.slice(file.mimetype.indexOf('/') + 1);
            console.log("ext",ext)
            if(ext === "png" || ext === "jpeg" || ext === "jpg"){
                cb(null,'public/uploads/images')
            }else if(ext === "pdf" || ext === "doc"){
                cb(null,'public/uploads/resume')
            }else{
                cb(null,'public/uploads/multiplefiles')
            }
        }
    },
    filename:function(req,file,cb){
        const name = Date.now()+'-'+file.originalname;
        cb(null,name)
    }
})
const upload = multer({storage:storage})
module.exports = upload.fields([{name:'profile', maxCount:1},{name:'resume', maxCount:1},{name:'multiplefiles',maxCount:8}])
