const User = require("../models/user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const registerUser  = async (req,res)=>{
    try{
        console.log(req.files)
        let extraFiles = []
        for(let idx in req.files.multiplefiles){
            let filename = req.files.multiplefiles[idx].filename
            extraFiles.push(filename)
        }
        console.log("extraFiles",extraFiles)
        let {fullName, email, password, phoneNumber,gender,country,radio,} = req.body;
        console.log(fullName, email, password, phoneNumber,gender,country,radio)
        const existingUser = await User.findOne({ email: email });
        if (existingUser)return res.status(400).json({ msg: "An account with this email already exists." });
    
        const salt = await bcrypt.genSalt();
        const passwordHash = await bcrypt.hash(password, salt);
        const newUser = new User({
            fullName,
            email,
            phoneNumber,
            password: passwordHash,
            gender: gender,
            country:country,
            remote_work:radio,
            image_path:req.files.profile[0].filename,
            resume_path:req.files.resume[0].filename,
            extraFiles:extraFiles
        });
        
        const savedUser = await newUser.save();
        res.status(200).json({
            "user": savedUser
        });
    }catch(err){
        console.log("Eroor while Registering",err)
        res.status(500).json({ error: err.message });
    }
}

const  loginUser = async(req,res)=>{
    try{
        console.log("herre")
        const { email, password } = req.body;
        const user = await User.findOne({ email: email });
        
        if (!user)return res.status(400).json({ msg: "No account with this email has been registered." });
        const isMatch = await bcrypt.compare(password, user.password);
    
        if (!isMatch) return res.status(400).json({ msg: "Invalid credentials." });
        const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);
        res.json({
            "token":token,
            "user": user
        });
    }catch(err){
        console.log("Eroor while Log In",err)
        res.status(500).json({ error: err.message });
    }
}

const validateToken = async (req, res) => {
    try {
        const token = req.header("x-auth-token");
        if (!token) return res.json(false);

        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if (!verified) return res.json(false);

        const user = await User.findById(verified.id);
        if (!user) return res.json(false);
        return res.json(true);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

const fetchUser = async (req, res) => {
    try{
        const user = await User.findById(req.user);
        res.json({
            "user":user,
            "token":req.header("x-auth-token")
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
   
}
const updateUser = async (req, res) => {
    try{
        console.log(req.body)
        const user = await User.findByIdAndUpdate(req.user,{$set: req.body});
        res.json({
            "user":user,
            "token":req.header("x-auth-token")
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
   
}

const updateFile = async (req, res) => {
    try{
        console.log(req.params.oldfile)
        await User.findByIdAndUpdate( 
            { _id: req.user }, 
            { $pull: { extraFiles: req.params.oldfile } } 
        )
        console.log(req.files)
        let filename = req.files.multiplefiles[0].filename
        console.log(filename)
        const user =  await User.findByIdAndUpdate( 
            { _id: req.user }, 
            { $push: { extraFiles: filename } } 
        )
        res.json({
            "user":user,
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
   
}

const removeFile = async (req, res) => {
    try{
        var filePath = `public/uploads/multiplefiles/${req.params.oldfile}`; 
        fs.unlinkSync(filePath)
        const user =  await User.findByIdAndUpdate( 
            { _id: req.user }, 
            { $pull: { extraFiles: req.params.oldImage } } 
        )
    }catch(err){
        console.log("Error in fetch user",err)
    }
   
}
const updateImage = async (req,res) =>{
    try{
        console.log( req.files)
        let filename = req.files.profile[0].filename
        const user = await User.findByIdAndUpdate(req.user,{$set: {"image_path":filename}});
        res.json({
            "user":user,
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
}
module.exports = {registerUser,loginUser,validateToken,fetchUser,updateUser,updateFile,updateImage}