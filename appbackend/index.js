
const express =require('express');
const bodyParser =require('body-parser');
const mongoose =require('mongoose');
const cors =require('cors');
require('dotenv').config()
const app = express();
const router =require('./routes/index.js');
const port = process.env.PORT || 3200
console.log(process.env.PORT)


app.use(express());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());
app.use(express.static(`${__dirname}/public`));

app.listen(port,()=>{
	console.log(`Server is running......${port}`)
})


app.use('/api', router);
mongoose.connect(process.env.MONGODB_STRING, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

mongoose.connection.once('open',()=>{
    console.log("MongoDB database connection established successfully")
})