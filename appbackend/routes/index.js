const Router = require('express');
const auth = require('../middlewares/auth')
const router = Router();
const upload = require('../middlewares/uploadfile')
const {registerUser,validateToken,loginUser,fetchUser,updateUser,updateFile,updateImage} = require("../controllers/authController.js")
router.post('/signup',upload,registerUser)
router.post('/login',loginUser)
router.post('/tokenIsValid',validateToken)
router.post('/updateUser',auth,updateUser)
router.get('/fetchuser',auth,fetchUser)
router.post('/updatefile/:oldfile',auth,upload,updateFile)
router.post('/updateprofile/:oldImage',auth,upload,updateImage)



module.exports = router;
