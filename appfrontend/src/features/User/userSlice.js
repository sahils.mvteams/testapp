import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios'
export const signupUser = createAsyncThunk(
  'users/signupUser',
  async ({ fullName, email, password, phoneNumber,gender,profile,resume,country,radio,multipleFiles}, thunkAPI) => {
    try {
        let formdata =  new FormData()
        formdata.append("fullName",fullName)
        formdata.append("email",email)
        formdata.append("password",password)
        formdata.append("phoneNumber",phoneNumber)
        formdata.append("gender",gender)
        formdata.append("profile",profile)
        formdata.append("resume",resume)
        formdata.append("country",country)
        formdata.append("radio",radio)
        for (let i = 0; i < multipleFiles.length; i++) {
          formdata.append('multiplefiles', multipleFiles[i]);
        }

        let signupresp = await axios.post("http://localhost:3100/api/signup",formdata)
        // let loginresp = await axios.post("http://localhost:3100/api/login",{email,password})

        if (signupresp.status === 200) {
            console.log("signupresp",signupresp)
            return { ...signupresp.data};
        } else {
            return thunkAPI.rejectWithValue(signupresp.data);
        }
    } catch (e) {
      console.log(e.response.data)
      return thunkAPI.rejectWithValue(e.response.data.msg);
    }
  }
);

export const loginUser = createAsyncThunk(
  'users/login',
  async ({ email, password }, thunkAPI) => {
    try {
        let loginresp = await axios.post("http://localhost:3100/api/login",{email,password})
        console.log('loginresp', loginresp);
        if (loginresp.status === 200) {
            localStorage.setItem('token', loginresp.data.token);
            localStorage.setItem('isAuthenticated', true);
            return { ...loginresp.data};
        } else {
            return thunkAPI.rejectWithValue(loginresp.data);
        }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data.msg);
    }
  }
);

export const fetchUserBytoken = createAsyncThunk(
  'users/fetchUserByToken',
  async ({ token }, thunkAPI) => {
    try {
     const fetchUser = await axios.get("http://localhost:3100/api/fetchUser",{headers: {"x-auth-token": token}})
      if (fetchUser.status === 200) {
        return { ...fetchUser.data };
      } else {
        return thunkAPI.rejectWithValue(fetchUser.data);
      }
    } catch (e) {
      console.log('Error', e.response.data);
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const updateUser = createAsyncThunk(
  'users/updateUser',
  async ({ token ,updatedData}, thunkAPI) => {
    try {
      console.log("All info",token,updatedData)
     let updatedUser = await axios.post("http://localhost:3100/api/updateUser",updatedData,{headers: {"x-auth-token": token}})
  
      if (updatedUser.status === 200) {
        return { ...updatedUser.data };
      } else {
        return thunkAPI.rejectWithValue(updatedUser.data);
      }
    } catch (e) {
      console.log('Error', e.response.data);
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);
export const updateExtraFile = createAsyncThunk(
  'users/updateExtraFile',
  async ({ token,newfile,oldfile }, thunkAPI) => {
    try {
      let formdata =  new FormData()
      formdata.append("multiplefiles",newfile)
      formdata.append("oldfile",oldfile)
     let updateFile = await axios.post(`http://localhost:3100/api/updatefile/${oldfile}`,formdata,{headers: {"x-auth-token": token}})
      if (updateFile.status === 200) {
        return { ...updateFile.data };
      } else {
        return thunkAPI.rejectWithValue(updateFile.data);
      }
    } catch (e) {
      console.log('Error', e.response.data);
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const updateProfle = createAsyncThunk( 
  'users/updateUserProfile',
  async ({ token,newImage,oldImage }, thunkAPI) => {
    try {
      console.log("profile",newImage)
      console.log("oldProfile",oldImage)
      let formdata =  new FormData()
      formdata.append("profile",newImage)
     let updateImage = await axios.post(`http://localhost:3100/api/updateprofile/${oldImage}`,formdata,{headers: {"x-auth-token": token}})
      if (updateImage.status === 200) {
        return { ...updateImage.data };
      } else {
        return thunkAPI.rejectWithValue(updateImage.data);
      }
    } catch (e) {
      console.log('Error', e.response.data);
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
)

export const userSlice = createSlice({
  name: 'user',
  initialState: {
   user:{}
  },
  reducers: {
   
  },
  extraReducers: {
    [signupUser.fulfilled]: (state, { payload }) => {
      state.user = payload.user;
      return state;
    },
    [signupUser.rejected]: (state, { payload }) => {
      console.log("payload",payload)
      state.errorMessage = payload.msg;
    },
    [loginUser.fulfilled]: (state, { payload }) => {
      console.log("payload",payload)
      state.user = payload.user
      return state;
    },
    [loginUser.rejected]: (state, { payload }) => {
      state.errorMessage = payload.msg;
    },
    [fetchUserBytoken.fulfilled]: (state, { payload }) => {
      state.user = payload.user
    },
    [fetchUserBytoken.rejected]: (state,{payload}) => {
      state.errorMessage = payload
    },
    [updateExtraFile.fulfilled] : (state,{payload})=>{
      console.log("HEREE")
      state.user = payload.user
      return state;
    },
    [updateUser.fulfilled]: (state, { payload }) => {
      console.log("here reached")
      state.user = payload.user
      return state;
    },
    [updateProfle.fulfilled]: (state, { payload }) => {
      console.log("here reached")
      state.user = payload.user
      return state;
    },
  },
});

export const { clearState } = userSlice.actions;

export const userSelector = (state) => state.user;