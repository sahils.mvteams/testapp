import React, { useState } from 'react'
import {Dropdown,Row,Col,InputGroup,Form,Button} from 'react-bootstrap';
import { useNavigate} from 'react-router-dom';
import '../index.css'
import {FaRegUser} from "react-icons/fa";

function Header() {
    const navigate = useNavigate()
    const logOut = ()=>{
        localStorage.removeItem('token')
        localStorage.removeItem('isAuthenticated')
        navigate('/login')
    }
    return (
        <>  
            <Row className='mt-2'>
                <Col md={2} sm={2} xs={2} className='mt-2 mb-2'>
                    <span style={{color:"white", fontSize:"28px", fontWeight:"bold", paddingBottom:"20px", cursor:"pointer"}} onClick={()=>navigate('/')}>TEST APP</span>
                </Col>
                <Col md={6} sm={6} xs={6} className='mt-2 mb-2'>
                {/* <InputGroup>
                    <Form.Control 
                    required
                    type="text"
                    placeholder="Search"
            
                    />
                </InputGroup> */}
                </Col>
                <Col md={2} sm={2} xs={2} className='mt-2 mb-2'>
                    {/* <Button className="btn btn-primary" >Search</Button> */}
                </Col>
                <Col md={2} sm={2} xs={2} className='mt-2 mb-2 ' style={{textAlign:"right"}} >
                    <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic">
                        <FaRegUser/>
                    </Dropdown.Toggle>   
                    <Dropdown.Menu>
                        <Dropdown.Item onClick={logOut}>Logout</Dropdown.Item>
                        <Dropdown.Item onClick={()=>{navigate('/viewprofile')}}>Your Profile</Dropdown.Item>
                    </Dropdown.Menu>
                    </Dropdown>
                    
                </Col>
            </Row>
        </>

    );
}

export default Header;