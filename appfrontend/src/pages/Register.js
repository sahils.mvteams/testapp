import React,{useState} from 'react'
import {Container,Row,Col, Card,Form, Button} from 'react-bootstrap' 
import {Link} from "react-router-dom"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { signupUser } from '../features/User/userSlice'
import "../style.css" 
import {checkEmail,checkPhone} from "../utils/index"
import { openNotification } from '../utils/index'
// import { NotificationManager } from 'react-notifications';
function Regiser(){
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const [fullName, setFullName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [error, setError] = useState(false)
    const [gender, setGender] = useState('')
    const [profile, setProfile] = useState('')
    const [profileError, setProfileError] = useState(false)
    const [resume, setResume] = useState('')
    const [resumeError, setResumeError] = useState(false)
    const [country, setCountry] = useState('')
    const [radio, setRadio] = useState('no')
    const [errorMsg, setErrorMsg] = useState("")
    const [multipleFiles, setMultiplefiles]= useState([])
    const handleRegister = async(e)=>{
          e.preventDefault()
          try{
            console.log(profile,resume)
              let validation = await handleValidation(fullName, email, password, phoneNumber,gender,profile,resume,country,radio,multipleFiles)
              if(validation){
                setError(false)
                setErrorMsg("")
                dispatch(signupUser({fullName, email, password, phoneNumber,gender,profile,resume,country,radio,multipleFiles})).then(async (res)=>{
                  console.log("response",res)
                  if(res.error && res.error.message === "Rejected"){
                    setError(true)
                    setErrorMsg(res.payload)
                  }else{
                    await openNotification('Success','Registered Successfully')
                    navigate('/login')
                  }
                  
                })
              }
          }catch(err){
              console.log("Error while Registering User",err)
          }
     
      }
      
      const handleValidation = async(fullName, email, password, phoneNumber,gender,profile,resume,country,radio,multipleFiles)=>{
        console.log(fullName, email,password,phoneNumber,gender,profile,resume,country,radio)
        console.log("multipleFiles",multipleFiles)
        if(!fullName || !email || !password || !phoneNumber || !gender || !profile | !resume || !country || !radio || !multipleFiles.length > 0){
          setError(true)
          setErrorMsg("Fill all fields")
          return false
        }
        let validatEmail = await checkEmail(email)
        if(!validatEmail){
          setError(true)
          setErrorMsg("Enter valid mail")
          return false
        }
        let validatePhone = await checkPhone(phoneNumber)
        if(!validatePhone){
          console.log("here")
          setError(true)
          setErrorMsg("Invalid phone number")
          return false
        }
        return true
      }

      const handleName=(e)=>{
        setError("")
        setErrorMsg("")
        setFullName(e.target.value)
      }
      const handleEmail=(e)=>{
        setError("")
        setErrorMsg("")
        setEmail(e.target.value)
      }
      const handlePassword=(e)=>{
        setError("")
        setErrorMsg("")
        setPassword(e.target.value)
      }
      const handlePhone=(e)=>{
        setError("")
        setErrorMsg("")
        setPhoneNumber(e)
      }

      const handleCheckGender = (e)=>{
        console.log(e.target.checked)
        console.log(e.target.value)
        setGender(e.target.value)
      }

      const handleCountry = (e)=>{
        console.log(e.target.value)
        setCountry(e.target.value)
      }

      const handleProfile = (e)=>{
        let file = e.target.files[0]
        if (file?.type === 'image/png' || file?.type === 'image/jpeg' || file?.type === 'image/jpg') {
          console.log(file)
          setProfile(file)
          setProfileError(false)
        }else{
          setProfileError(true)
        }

      }

      const handleResume = (e)=>{
        let file = e.target.files[0]
        if (file?.type) {
          const extension = file.type.split("/")[1]; // "pdf"
          const regex = /^pdf$|^doc$/;
          const isValidFile = regex.test(extension);
          if(isValidFile){
            setResume(file)
            setResumeError(false)
          }else{
            setResumeError(true)
          }
        }
      }

      const handleRadio = (e)=>{
        console.log(e.target.checked)
        if(e.target.checked){
          setRadio('yes')
        }else{
          setRadio('no')
        }
      }

      const handleMultipleFiles = (e)=>{
        console.log(e.target.files)
        let files = e.target.files
        setMultiplefiles(e.target.files)
      }
	return(
            <div>
              <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                  <Col md={8} lg={6} xs={12}>
                    <Card className="px-4">
                      <Card.Body>
                        <div className="mb-3 mt-md-4">
                          <h2 className="fw-bold mb-2 text-center text-uppercase ">
                            Register Here
                          </h2>
                          {error && <h5 className="text-center" style={{color:"red"}}>{errorMsg}</h5>}
                          <div className="mb-3">

                            <Form onSubmit={handleRegister}>
                              <Form.Group className="mb-3" >
                                <Form.Label className="text-center">Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter Name"  onChange={handleName} />
                              </Form.Group>
        
                              <Form.Group className="mb-3" >
                                <Form.Label className="text-center">
                                  Email address
                                </Form.Label>
                                <Form.Control type="email" placeholder="Enter email"  onChange={handleEmail}/>
                              </Form.Group>

                              <Form.Group
                                className="mb-3"
                              >
                                <Form.Label> Phone</Form.Label>
                                    <PhoneInput
                                    inputStyle={{width:"100%"}}
                                    onlyCountries={["in"]}
                                    country={'in'}
                                    value={phoneNumber}
                                    onChange={handlePhone}
                                    />
                              </Form.Group>

                             
                              <div className='col-md-12 mb-3'>

                                {['Male','Female','Other'].map((ele)=>{
                                  return(
                                    <Form.Group key ={ele} className='col-md-4' style={{display:'inline'}} > 
                                      <Form.Check
                                        inline
                                        label={ele}
                                        value={ele}
                                        name="group1"
                                        type="radio"
                                        onChange={handleCheckGender}
                                      />
                                    </Form.Group> 
                                  )

                                })}
                                </div> 
                              
                             <Form.Group
                                className="mb-3"
                              >
                              <Form.Label> Country</Form.Label>
                                <Form.Select className="mb-3" onChange={handleCountry}>
                                  <option >Select Country</option>
                                  <option>India</option>
                                  <option>Russia</option>
                                  <option>Israel</option>
                                </Form.Select>
                              </Form.Group>

                              <Form.Group className="mb-3">
                                <Form.Label>Upload Image</Form.Label>
                                <Form.Control type="file" onChange={handleProfile}/>
                                {profileError && <span style={{color:'red'}}>*Only .png, .jpg and .jpeg supported</span>}
                              </Form.Group>

                              <Form.Group
                                className="mb-3"
                              ></Form.Group>

                              <Form.Group className="mb-3">
                                <Form.Label>Upload Resume</Form.Label>
                                <Form.Control type="file" onChange={handleResume} />
                                {resumeError && <span style={{color:'red'}}>*Only .pdf and .doc supported</span>}
                              </Form.Group>

                              <Form.Group
                                className="mb-3"
                              >
                                     
                              <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password"  onChange={handlePassword} />
                              </Form.Group>

                              <Form.Group controlId="formFileMultiple" className="mb-3">
                                <Form.Label>Upload multiple files</Form.Label>
                                <Form.Control type="file" multiple onChange={handleMultipleFiles} />
                              </Form.Group>
                              
                              <Form.Group className="mb-3" >
                                <Form.Check type="checkbox" label="Open to remote work?" onChange={handleRadio}/>
                              </Form.Group>


                              <div className="d-grid">
                                <Button variant="primary" type="submit" >
                                  Create Account
                                </Button>
                              </div>
                            </Form>
                            <div className="mt-3">
                              <p className="mb-0  text-center">
                                Already have an account??{' '}
                                <Link to="/login">
                                  Sign In
                                </Link>
                              </p>
                            </div>
                          </div>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>
            </div>
          );
}
export default Regiser;