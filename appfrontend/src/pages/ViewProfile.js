import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Card, Form, Button,Modal} from 'react-bootstrap'
import { useNavigate, Link } from "react-router-dom"
import { userSelector, fetchUserBytoken, updateExtraFile,updateProfle} from '../features/User/userSlice'
import { useDispatch, useSelector } from 'react-redux';
import{openNotification} from '../utils/index'
import "../style.css";
import 'react-phone-input-2/lib/style.css';
import Header from '../components/Header';
let updatedData = {}
function ViewProfile() {
    const [updateFile, setUpdateFile] = useState('')
    const [fileToUpdate, setFileToUpdate] = useState('')
    const [newProfile, setNewProfile] = useState('')
    const [oldProfile, setOldProfile] = useState('')
    const [show, setShow] = useState(null)
    const [showImage,setShowImage] = useState(null)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { user } = useSelector(
        userSelector
    );
    useEffect(() => {
        dispatch(fetchUserBytoken({ token: localStorage.getItem('token') }));
    }, [show,showImage]);

    const handleClose = () => setShow(false);
   const handleCloseImage = () => setShowImage(false)

    const handleFileUpdate= (e, ele)=>{
        setFileToUpdate(ele)
        setUpdateFile(e.target.files[0])
        setShow(true)
    }

    const handleUpdate = async()=>{
        console.log('fileToUpdate',fileToUpdate)
        dispatch(updateExtraFile({token:localStorage.getItem('token'), "newfile":updateFile , "oldfile":fileToUpdate}))
            // console.log("response",res)
            setShow(false)
            setFileToUpdate('')
            setFileToUpdate('')
            // if(res.error && res.error.message === "Rejected"){
            //     await openNotification('Error',res.payload)
            // }else{
                await openNotification('Success','Updated Successfully')
                navigate('/viewprofile')
            // }
            setShow(false)
            setFileToUpdate('')
            setFileToUpdate('')
    }

    const handleChangeImage = (e,ele)=>{
        console.log("hereeeee")
        setNewProfile(e.target.files[0])
        setOldProfile(ele)
        setShowImage(true)
        
    }
    
    const handleUpdateImage = async ()=>{
        console.log("oldProfile",oldProfile)
        console.log("newProfile",newProfile)
        console.log("showImage",showImage)
        dispatch(updateProfle({token:localStorage.getItem('token'), "newImage":newProfile , "oldImage":oldProfile}))
        await openNotification('Success','Updated Successfully')
        navigate('/viewprofile')
        setNewProfile('')
        setOldProfile('')
        setShowImage(false)
    }
    return (
        <div>
             <Modal show={show} >
                <Modal.Header closeButton>
                <Modal.Title>Update file</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you really want to update your file?</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleUpdate}>
                    Update
                </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showImage} >
                <Modal.Header closeButton>
                <Modal.Title>Update file</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you really want to update your profile?</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseImage}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleUpdateImage}>
                    Update
                </Button>
                </Modal.Footer>
            </Modal>
             <Container fluid style={{backgroundColor:"black"}}>
                <Row>
                    <Col md={12} className=' nav-bar' >
                        <Header/>
                    </Col>
                </Row>
            </Container>
            <div className="container">
            <div className="main-body mt-5">
                <div className="row gutters-sm">
                    <div className="col-md-12">
                        <div className="card mb-3" >
                            <div className="card-body row" style={{padding:'unset'}}>
                                <div className='col-10' style={{ padding: 'var(--bs-card-spacer-y) var(--bs-card-spacer-x)', borderRight:'1px solid lightgrey'}}> 
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">Full Name</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        {user.fullName}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">Email</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        {user.email}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">Phone</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        +{user.phoneNumber}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">country</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        {user.country}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">Gender</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        {user.gender}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3">
                                        <h6 className="mb-0">Open to remote work?</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                        {user.remote_work}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3 mt-2">
                                        <h6 className="mb-0">Resume</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            <div className="col-sm-12 text-secondary">
                                                <a href={`http://localhost:3100/uploads/resume/${user.resume_path}`}
                                                download="Resume"
                                                rel="noreferrer"
                                                >
                                                    <button className="btn btn-info">Download</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-3 mt-2">
                                        <h6 className="mb-0">Extra files</h6>
                                        </div>
                                        <div >
                                        {user?.extraFiles && 
                                            user.extraFiles.map((ele,idx)=>{
                                            return(
                                                <>
                                                <div className='col-md-12 row'>
                                                    <div className='col-md-3 mt-2'>
                                                    <span style={{paddingLeft:'5px'}}>{ele.slice(ele.indexOf('-') + 1)}</span>
                                                    </div>
                                                    {/* <div className = "col-md-3 mt-2" style={{textAlign:'right'}}>
                                                        <button className='btn btn-danger'>Delete</button>
                                                    </div> */}
                                                    
                                                    <div className = "col-md-3 mt-2" style={{textAlign:'right'}}>
                                                        <button className="btn btn-info" >
                                                            <label htmlFor="test">Upload</label></button>
                                                        <input type="file" id="test" hidden  onChange={(e) => handleFileUpdate(e, ele)}/>
                                                    </div>
                                                    <div className = "col-md-3 mt-2" style={{textAlign:'right'}}>
                                                        <a href={`http://localhost:3100/uploads/multiplefiles/${ele}`}
                                                        download="Resume"
                                                        rel="noreferrer"
                                                        >
                                                        <button className="btn btn-info">Download</button>
                                                        </a>
                                                    </div>
                                                </div>
                                                <hr/>
                                                </>
                                                )
                                            })
                                            }
                                        </div>
                                       
                                    </div>
                                    
                                    <div className="row">
                                        <div className="col-sm-12" style={{textAlign:'center'}}>
                                        <button onClick={()=>{navigate('/update-profile')}} className='btn btn-primary'>Edit profile Info</button>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-2' style={{margin:"auto"}} >
                                    <div style={{textAlign:'center'}} className='col-12'>
                                        <img src={`http://localhost:3100/uploads/images/${user.image_path}`} alt="Admin" style={{border:'0.5px solid lightgrey'}} className="rounded-circle" width={150}/>
                                        <div className="mt-3" style={{textAlign:'center'}}>
                                            <h4>{user.fullName}</h4>
                                        </div>
                                        <div className = "col-md-12 mt-2" style={{textAlign:'center'}}>
                                            <button className="btn btn-info" >
                                                <label htmlFor="test1">Upload Image</label></button>
                                            <input type="file" id="test1" hidden  onChange={(e) => handleChangeImage(e, user.image_path)}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    );
}
export default ViewProfile;