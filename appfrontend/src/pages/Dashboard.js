import React,{useEffect} from 'react'
import {Row,Col,Container} from 'react-bootstrap'
import Header from '../components/Header';
import { useSelector, useDispatch } from 'react-redux';
import { userSelector,fetchUserBytoken} from '../features/User/userSlice';

function Dashboard(){
    const dispatch = useDispatch()
    const { user} = useSelector(userSelector);
    useEffect(() => {
        dispatch(fetchUserBytoken({ token: localStorage.getItem('token') }));
      }, []);
    console.log("User",user)
	return(
    <>
        <Container fluid style={{backgroundColor:"black"}}>
            <Row>
                <Col md={12} className=' nav-bar' >
                    <Header/>
                </Col>

            </Row>

        </Container>
        <h1 style={{textAlign:'center'}}>Home</h1>

    </>
	)
}
export default Dashboard;