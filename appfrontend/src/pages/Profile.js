import React,{useEffect, useState} from 'react'
import {Container,Row,Col, Card,Form, Button} from 'react-bootstrap' 
import {Link} from "react-router-dom"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { useNavigate } from 'react-router-dom'
import { signupUser } from '../features/User/userSlice'
import "../style.css" 
import Header from '../components/Header'
import {checkEmail,checkPhone} from "../utils/index"
import { openNotification } from '../utils/index'
import { userSelector,fetchUserBytoken,updateUser } from '../features/User/userSlice'
import {useDispatch,useSelector } from 'react-redux';
let updateObj={}
function Regiser(){
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const [error, setError] = useState(false)
    const [errorMsg, setErrorMsg] = useState("")
    const { user} = useSelector(
        userSelector
    );
    const [userdata, setUserData] = useState()
    useEffect(() => {
        dispatch(fetchUserBytoken({ token: localStorage.getItem('token') })).then(res=>{
            console.log("res",res.payload.user)
            setUserData(res.payload.user)
        });
    }, []);


   
    const handleUpdate = async(e)=>{
        e.preventDefault()
        try{
       
            let validation = await handleValidation(updateObj)
              console.log("updatedData",updateObj)
              if(validation){
                  dispatch(updateUser({token:localStorage.getItem('token'), "updatedData":updateObj})).then(async (res)=>{
                    updateObj={}
                    console.log("response",res)
                    if(res.error && res.error.message === "Rejected"){
                      setError(true)
                      setErrorMsg(res.payload)
                    }else{
                        await openNotification('Success',"Updated Successfully")
                        navigate('/viewprofile')
                    } 
                  }) 
              }
        }catch(err){
            console.log("Error in handleUpdate",err)
        }
    }

    const handleValidation = async(updatedData)=>{
      if(Object.keys(updatedData).length > 0){
        for (const key in updatedData) {
            if (updatedData.hasOwnProperty(key) && updatedData[key].trim() === '') {
                setError(true)
                setErrorMsg("Fill all fields")
                return false
            }
        }
        if(updatedData?.email){
            let validateMail = await checkEmail(updatedData.email)
            if(!validateMail){
                return false
                setError(true)
                setErrorMsg("Invalid Email")
            }
        }
        return true;
      }else{
        setError(true)
        setErrorMsg("Nothing to update")
        return false
      }
    }


      const handleName=(e)=>{
        setError("")
        setErrorMsg("")
        updateObj.fullName = e.target.value
      }
      const handleEmail=(e)=>{
        setError("")
        setErrorMsg("")
        updateObj.email = e.target.value
      }
      const handlePhone=(e)=>{
        setError("")
        setErrorMsg("")
        updateObj.phoneNumber = e
      }

      const handleCheckGender = (e)=>{
        setError("")
        setErrorMsg("")
        updateObj.gender = e.target.value
      }

      const handleCountry = (e)=>{
        setError("")
        setErrorMsg("")
        updateObj.country = e.target.value
      }

      const handleRadio = (e)=>{
        setError("")
        setErrorMsg("")
        if(e.target.checked){
            updateObj.remote_work = "yes"
        }else{
            updateObj.remote_work = "no"
        }
      }
	return(
            <div>
                 <Container fluid style={{backgroundColor:"black"}}>
                    <Row>
                        <Col md={12} className=' nav-bar' >
                            <Header/>
                        </Col>
                    </Row>
                </Container>
              {userdata && <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                  <Col md={8} lg={6} xs={12}>
                    <Card className="px-4">
                      <Card.Body>
                        <div className="mb-3 mt-md-4">
                          <h2 className="fw-bold mb-2 text-center text-uppercase ">
                            Update Profile
                          </h2>
                          {error && <h5 className="text-center" style={{color:"red"}}>{errorMsg}</h5>}
                          <div className="mb-3">

                            <Form onSubmit={handleUpdate}>
                              <Form.Group className="mb-3" >
                                <Form.Label className="text-center">Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter Name"  onChange={handleName} defaultValue={userdata.fullName}/>
                              </Form.Group>
        
                              <Form.Group className="mb-3" >
                                <Form.Label className="text-center">
                                  Email address
                                </Form.Label>
                                <Form.Control type="email" placeholder="Enter email"  onChange={handleEmail} defaultValue={userdata.email}/>
                              </Form.Group>

                             {userdata.phoneNumber && <Form.Group
                                className="mb-3"
                              >
                                <Form.Label> Phone</Form.Label>
                                    <PhoneInput
                                    inputStyle={{width:"100%"}}
                                    onlyCountries={["in"]}
                                    country={'in'}
                                    onChange={handlePhone}
                                    defaultValue={userdata.phoneNumber}
                                    value={userdata.phoneNumber.toString()}
                                    />
                              </Form.Group>}

                             
                              <div className='col-md-12 mb-3'>

                                {['Male','Female','Other'].map((ele)=>{
                                  return(
                                    <Form.Group key ={ele} className='col-md-4' style={{display:'inline'}} > 
                                      <Form.Check
                                        inline
                                        label={ele}
                                        value={ele}
                                        name="group1"
                                        type="radio"
                                        defaultChecked={userdata.gender === ele ? true : false}
                                        onChange={handleCheckGender}
                                      />
                                    </Form.Group> 
                                  )

                                })}
                                </div> 
                              
                            { userdata.country && <Form.Group
                                className="mb-3"
                              >
                              <Form.Label> Country</Form.Label>
                                <Form.Select className="mb-3" onChange={handleCountry} defaultValue={userdata.country}>
                                  <option disabled>Select Country</option>
                                  <option value="India">India</option>
                                  <option value="Russia">Russia</option>
                                  <option value="Israel">Israel</option>
                                </Form.Select>
                              </Form.Group>}

                              <Form.Group className="mb-3" >
                                <Form.Check type="checkbox" label="Open to remote work?" onChange={handleRadio} defaultChecked={userdata.remote_work === 'yes' ? true : false}/>
                              </Form.Group>

                              <div className='col-md-12' style={{textAlign:'center'}}>
                                <Button variant="primary" type="submit" >
                                  Update
                                </Button>
                              </div>
                            </Form>

                          </div>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>}
            </div>
          );
}
export default Regiser;