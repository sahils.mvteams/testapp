import React from 'react'
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard.js'
import {BrowserRouter,Routes,Route,useNavigate} from 'react-router-dom'
import PrivateRoute from './PrivateRoute';
import ProtectedRoute from  './ProtectedRoute'
import ViewProfile from "./pages/ViewProfile"
import Profile from './pages/Profile'
import 'react-notifications/lib/notifications.css';
import { NotificationContainer } from 'react-notifications';
import "./style.css"

function App() {
  return (
    <>
      <NotificationContainer />
      <BrowserRouter>
        <Routes>    
          <Route exact path='/' 
            element={
              <PrivateRoute>
                <Dashboard/>
              </PrivateRoute>
            }/> 
            <Route exact path='/viewprofile' 
            element={
              <PrivateRoute>
                <ViewProfile/>
              </PrivateRoute>
            }/> 
             <Route exact path='/update-profile' 
            element={
              <PrivateRoute>
                <Profile/>
              </PrivateRoute>
            }/>
            
          <Route exact path='/register' 
            element={
              <ProtectedRoute>
                <Register/>
              </ProtectedRoute>
            }/> 
          <Route exact path='/login' 
          element={
            <ProtectedRoute>
              <Login/>
            </ProtectedRoute>
          }/>    
                            
        </Routes>
      </BrowserRouter>
    </>

  );
}

export default App;